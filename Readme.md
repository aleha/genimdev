<!-- Readme.md is generated from Readme.org. Please edit that file -->


# genimdev R package

This package provides infrastructure to use different image devices (that might produce different output formats) with the same function.

This is handy to change all images generated in a script/report from PDF to, say, SVG.


## Example Usage

Usage is (should be) extremely simple:

Load the package:

```R
library("genimdev")
```

Have a look at the registered devices / supported formats:

```R
genim_listdevs()
```

Check the currently selected format:

```R
genim_getcurrentformat()
```

Select one of the devices:

```R
genim_setformats("svg")
```

All following plots (when the graphics device is opened via \`genimg\`) will be SVGs:

```R
genimg("test")
plot(1:10, 1:10)
dev.off()
## -> SVG generated
```


## Installation

Installation directly from gitlab is possible, when the package [devtools](https://cran.r-project.org/package=devtools) is installed.

You can install this package with

```R
library("devtools")
install_git("https://gitlab.gwdg.de/aleha/genimdev")
```


## Roadmap

Next steps include:

-   support for multiple selected output formats

<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->